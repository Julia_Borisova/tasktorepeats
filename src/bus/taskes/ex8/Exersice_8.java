package bus.taskes.ex8;

import java.util.Scanner;

/**
 * Напишите программу, которая будет проверять является ли число типа double целым.
 */
public class Exersice_8 {static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите число:");
        double number =scanner.nextDouble();

        if (number % 1 == 0) {
            System.out.println("Число целое.");
        } else {
            System.out.println("Число не целое.");
        }
    }
}
