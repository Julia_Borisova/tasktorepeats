package bus.taskes.ex12;

import java.util.Scanner;

/**
 * Напишите программу,
 * которая будет считать количество часов,
 * минут и секунд в n-ном количестве суток.
 */
public class Exersice_12 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int day;
        int hour;
        int minute;
        int second;
        do {
            System.out.print("Введите количество суток:");
            day = scanner.nextInt();
        } while (day < 0);
        hour=24*day;
        minute=60*hour;
        second=60*minute;
        System.out.println("Количество дней:" + day + " Часов:" + hour + " Минут:" + minute + " Секунд:" + second);
    }
}
