package bus.taskes.ex9;

import java.util.Scanner;

/**
 * Создайте метод, который в качестве аргумента получает
 * число и полностью обнуляет столбец прямоугольной матрицы,
 * который соответствует заданному числу.
 */
public class Exersice_9 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите размер матрицы:");
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        int number;
        do {
            System.out.println("Введите число:");
            number = scanner.nextInt();
        } while (m < number | number < 0 | m == number );


        int[][] array = new int[n][m];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = (int) (Math.random() * 10);
                if (number == j) {
                    array[i][j] = 0;
                }
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();

        }
    }
}
