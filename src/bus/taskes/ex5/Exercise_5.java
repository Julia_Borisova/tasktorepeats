package bus.taskes.ex5;

/**
 * Напишите программу, которая вычислит простые числа в пределах от 2 до 100.
 */

public class Exercise_5 {
    public static void main(String[] args) {
        for (int i = 2; i <= 100; i++) {
            if (isPrime(i)){
                System.out.print(" " + i);
            }
        }
    }

    public static boolean isPrime(int n) {
        if (n < 2) return false;
        for (int i = 2; i * i <= n; i++)
            if (n % i == 0) return false;
        return true;
    }
}
