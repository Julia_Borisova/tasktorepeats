package bus.taskes.ex13;

import java.util.Scanner;

/**
 * Из двумерного массива заполненного случайными числами
 * перенесите построчно эти числа в одномерный массив.
 */
public class Exersice_13 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите размер матрицы:");
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        int[][] array = new int[n][m];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = (int) (Math.random() * 10);
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }

        int d = n*m;
        int [] arrayA = new int[d];


        int y=0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                arrayA[y]=array[i][j];
                System.out.print(arrayA[y]);
            }
        }
    }

}
