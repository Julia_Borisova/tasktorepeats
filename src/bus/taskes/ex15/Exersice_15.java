package bus.taskes.ex15;

import java.util.Scanner;

/**
 *  В квадратной матрице [n][n] поменять столбцы и строки местами.
 */
public class Exersice_15 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите размер матрицы:");
        int n = scanner.nextInt();

        int [][] array= new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = (int)(Math.random() * 10);
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
        for (int j = 0; j < n; j++) {
            for (int i = 0; i < n; i++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
