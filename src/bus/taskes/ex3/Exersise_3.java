package bus.taskes.ex3;

import java.util.Scanner;

/**
 * Created by Петр on 21.02.2017.
 */
public class Exersise_3 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите курс евро:");
        double rate=scanner.nextDouble();
        System.out.println("Введите количество рублей:");
        double rub=scanner.nextDouble();
        rate=rub*rate;
        System.out.println(rub + " рублей" + '=' + rate + " евро" );

    }
}
