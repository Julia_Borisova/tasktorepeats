package bus.taskes.ex7;

import java.util.Scanner;

/**
 * Создайте простую игру основанную на угадывании чисел.
 Пользователь должен угадать загаданное число введя его в консоль.
 Если пользователь угадал число, то программа выведет «Правильно» и игра закончится,
 если нет, то пользователь продолжит вводить числа.
 Вывести «Загаданное число больше» - если пользователь ввел число меньше загаданного,
 «Загаданное число меньше»- если пользователь ввел число больше загаданного.
 */
public class Exersice_7 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int unknownNumber;
        int number;
        unknownNumber = (int) (Math.random()*100-100);
        do {
            System.out.print("Введите число:");
            number=scanner.nextInt();
            if (number == unknownNumber){
                System.out.println("Правильно!");
            } if (number > unknownNumber){
                System.out.println("Введенное число меньше.");
            } else {
                System.out.println("Введенное число больше.");
            }
        } while (number != unknownNumber);

    }
}
