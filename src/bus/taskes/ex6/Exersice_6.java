package bus.taskes.ex6;

import java.util.Scanner;

/**
 * Напишите программу, которая будет  выводить таблицу
 * умножения введеного пользователем с клавиатуры.
 */

public class Exersice_6 {
    static Scanner scanner= new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите число:");
        int number =scanner.nextInt();

        for(int i=2;i<=10;i++){
            System.out.println(number + "*" + i +"=" + number*i);
        }

    }
}
